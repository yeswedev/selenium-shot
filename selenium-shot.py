#!/usr/bin/python3

import csv, os, requests, shutil, sys, threading, time
from lxml import etree
from pyvirtualdisplay import Display
from selenium import webdriver

def take_screenshots_with_chromium(urls, width, height, directory, timestamp):
    driver = webdriver.Chrome(executable_path='./chromedriver')
    driver.set_window_size(width, height)
    for url in urls:
        driver.get(url)
        filename = directory + '/screenshot_' + timestamp + '_chrome_' + generate_string_from_url(url) + '_' + str(width) + 'x' + str(height) + '.png'
        driver.save_screenshot(filename)
    driver.quit()

def take_screenshots_with_firefox(urls, width, height, directory, timestamp):
    driver = webdriver.Firefox(executable_path='./geckodriver')
    driver.set_window_size(width, height)
    for url in urls:
        driver.get(url)
        filename = directory + '/screenshot_' + timestamp + '_firefox_' + generate_string_from_url(url) + '_' + str(width) + 'x' + str(height) + '.png'
        driver.save_screenshot(filename)
    driver.quit()

def take_screenshots(urls, size, directory, timestamp):
    width = size[0]
    height = size[1]
    display = Display(visible=0, size=(width, height))
    display.start()
    thread1 = threading.Thread(target = take_screenshots_with_chromium, args = (urls, width, height, directory, timestamp))
    thread2 = threading.Thread(target = take_screenshots_with_firefox, args = (urls, width, height, directory, timestamp))
    thread1.start()
    thread2.start()
    thread1.join()
    thread2.join()
    display.stop()

def generate_string_from_url(url):
    url_as_list = str.split(url, '/')[2:]
    url_as_list = list(filter(None, url_as_list))
    url_as_string = '_'.join(url_as_list)
    return url_as_string

def parse_csv(sizes, file, directory, timestamp):
    urls = []
    with open(file, newline='') as csvfile:
        csvreader = csv.reader(csvfile, delimiter=',', quotechar="'")
        for row in csvreader:
            urls.append(row[0])
    display_threads = []
    for size in sizes:
        display_threads.append(threading.Thread(target = take_screenshots, args = (urls, size, directory, timestamp)))
    for display_thread in display_threads:
        display_thread.start()
    for display_thread in display_threads:
        display_thread.join()

def parse_xml(sizes, sitemap, directory, timestamp):
    request = requests.get(sitemap)
    root = etree.fromstring(request.content)
    urls = []
    for sitemap in root:
        children = sitemap.getchildren()
        urls.append(children[0].text)
    display_threads = []
    for size in sizes:
        display_threads.append(threading.Thread(target = take_screenshots, args = (urls, size, directory, timestamp)))
    for display_thread in display_threads:
        display_thread.start()
    for display_thread in display_threads:
        display_thread.join()

timestamp = str(int(time.time()))
directory = 'screenshots_' + timestamp
if not os.path.exists(directory):
    os.makedirs(directory)
with open(sys.argv[1], newline='') as csvfile:
    csvreader = csv.reader(csvfile, delimiter=',', quotechar="'")
    sizes = []
    for row in csvreader:
        sizes.append(row)
if sys.argv[2].lower().endswith('.csv'):
    parse_csv(sizes, sys.argv[2], directory, timestamp)
elif sys.argv[2].lower().endswith('.xml'):
    parse_xml(sizes, sys.argv[2], directory, timestamp)
shutil.make_archive(directory, 'zip', directory)
shutil.rmtree(directory)
