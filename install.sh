#!/bin/sh
set -e

destination="$(dirname "$0")"
if [ ! -e "$destination/chromedriver" ]; then
	wget -O "$destination/chromedriver_linux64.zip" 'https://chromedriver.storage.googleapis.com/2.33/chromedriver_linux64.zip'
	unzip "$destination/chromedriver_linux64.zip" -d "$destination"
	rm "$destination/chromedriver_linux64.zip"
fi
if [ ! -e "$destination/geckodriver" ]; then
	wget -O "$destination/geckodriver-v0.19.0-linux64.tar.gz" 'https://github.com/mozilla/geckodriver/releases/download/v0.19.0/geckodriver-v0.19.0-linux64.tar.gz'
	tar xf "$destination/geckodriver-v0.19.0-linux64.tar.gz" -C "$destination"
	rm "$destination/geckodriver-v0.19.0-linux64.tar.gz"
fi

for package in python3-lxml python3-pyvirtualdisplay python3-selenium xvfb libgconf-2-4 chromium-browser firefox; do
	if [ -z "$(dpkg -l $package 2>/dev/null | grep "^ii  $package[^a-z0-9]")" ]; then
		sudo apt install --no-install-recommends $package
	fi
done
